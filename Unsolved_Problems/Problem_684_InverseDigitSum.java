package Unsolved_Problems;

import java.math.BigInteger;
import java.util.ArrayList;
import static Unsolved_Problems.EulerMethods.charToInt;
/* Define s(n) to be the smallest number that has a digit sum of n. For example s(10)=19.
Let S(k)=∑n=1ks(n). You are given S(20)=1074

Further let fi be the Fibonacci sequence defined by f0=0,f1=1 and fi=fi−2+fi−1 for all i≥2
Find ∑i=290S(fi)
Give your answer modulo 1000000007.
*/

public class Problem_684_InverseDigitSum
{
    public static void main(String[] args)
    {
        //ArrayList<Integer> fib = new ArrayList<>();
        ArrayList<String> sum = new ArrayList<>();
        int fib = 1, count = 0;
        BigInteger total = BigInteger.ZERO;

        //TODO Create memory efficient method to add numDigitSum 
        for (int i = 2; count < 51;) 
        {
            sum.add(numDigitSum(i));

            i += fib;
            fib = i - fib;
            //System.out.println(i);
            count++;
        }

        for (String x : sum)
        {
            BigInteger s = new BigInteger(x);
            total = total.add(s);
        }
        System.out.println(total.mod(BigInteger.valueOf(1000000007)));

    }

    public static int digitSum(int n) 
    {
        String s = String.valueOf(n);
        int digitSum = 0;

        for (int i = 0; i < s.length(); i++)
            digitSum += EulerMethods.charToInt(s.charAt(i));

        return digitSum;
    }

    static String numDigitSum (int n)
    {
        int x = n / 9;
        int y = n % 9;
        
        StringBuilder s = new StringBuilder();
        
        if (y != 0)
            s.append(String.valueOf(y));
        
        for(int z = 0; z < x; z++)
            s.append("9");

        return s.toString();

    }

    static String numDigitSum (BigInteger n)
    {
        BigInteger x = n.divide(BigInteger.valueOf(9));
        BigInteger y = n.mod(BigInteger.valueOf(9));
        
        StringBuilder s = new StringBuilder();
        
        if (y.equals(0))
            s.append(String.valueOf(y));
        
        for(int z = 0; x.compareTo(BigInteger.valueOf(z)) == 1; z++)
            s.append("9");

        return s.toString();

    }

}